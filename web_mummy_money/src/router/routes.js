// This is the way to lazy load components.
// Then, only when the route is hit, the component is loaded.
const Login = () => import('./../views/Login.vue');
const Dashboard = () => import('./../views/Dashboard.vue');

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: { requiresAuth: false },
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: Dashboard,
    meta: { requiresAuth: true },
  },
  {
    path: '*',
    redirect: '/dashboard'
  }
];

export default routes;
