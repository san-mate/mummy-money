import User from './../services/user';

function redirectIfNotLoggedIn (to, from, next) {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (User.isAuthenticated()) {
      next();
    } else {
      next('/login');
    }
  } else {
    next();
  }
}

function redirectIfLoggedIn (to, from, next) {
  if (to.fullPath === '/login' && User.isAuthenticated()) {
    next('/dashboard');
  } else {
    next();
  }
}

export const beforeGuards = [
  redirectIfNotLoggedIn,
  redirectIfLoggedIn,
];
