import VueRouter from 'vue-router';

import routes from './routes';
import * as guards from './guards';

const config = {
  routes,
  mode: 'history'
};

const router = new VueRouter(config);

guards.beforeGuards.forEach((guard) => router.beforeEach(guard));

export default router;
