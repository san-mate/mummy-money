// Cards
import ChartCard from "./Cards/ChartCard.vue";

// Tables
import UsersTable from "./Tables/UsersTable.vue";

export {
  ChartCard,
  UsersTable
};
