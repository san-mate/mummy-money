import Vue from 'vue'
import VueRouter from 'vue-router';
import axios from 'axios';

import User from './services/user';
import router from './router/router';
import store from './store/store';

import MaterialDashboard from "./material-dashboard";

import App from './App.vue'

Vue.use(VueRouter);
Vue.use(MaterialDashboard);

Vue.config.productionTip = false

axios.interceptors.request.use(function(config) {
  const token = User.getAuthHeader();
  if(token) {
    config.headers.Authorization = `Token ${token}`;
  }
  return config;
}, function(err) {
  return Promise.reject(err);
});

new Vue({
  router,
  store,
  created () {
    if (User.isAuthenticated()) {
      this.$store.dispatch('SET_USER_INFO');
    }
  },
  render: h => h(App),
}).$mount('#app')
