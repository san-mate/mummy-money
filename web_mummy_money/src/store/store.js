import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import authStore from './modules/authStore';

const config = {
  modules: {
    authStore
  }
};

const store = new Vuex.Store(config);

export default store;
