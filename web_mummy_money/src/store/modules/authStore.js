import User from './../../services/user';

const state = {
  isAuthenticated: false,
  username: null,
};

const getters = {

};

const actions = {
  'SET_USER_INFO' ({ commit }) {
    return User.getInfo().then((response) => {
      commit('SET_USER_INFO', response.data);
    });
  },
  'REMOVE_USER_INFO' ({ commit }) {
    return Promise.resolve().then(() => {
      commit('REMOVE_USER_INFO');
    });
  },
};

const mutations = {
  ['SET_USER_INFO'] (state, info) {
    state.isAuthenticated = true;
    state.username = info.user.username;
  },
  ['REMOVE_USER_INFO'] (state) {
    state.isAuthenticated = false;
    state.username = null;
  },
};

export default {
  state,
  getters,
  actions,
  mutations
}
