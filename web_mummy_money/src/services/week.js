import axios from 'axios';
import API_ENDPOINT from './endpoint';

axios.defaults.baseURL = API_ENDPOINT;

export default {
  simulateWeeks (weeks) {
    return axios.post('weeks/simulate_weeks/', weeks);
  },
};
