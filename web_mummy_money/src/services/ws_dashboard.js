import WS_ENDPOINT from './ws_endpoint';

let socket = null;

export default {
  connect (onMessage) {
    socket = new WebSocket(WS_ENDPOINT);
    socket.onopen = () => {
      socket.onmessage = ({data}) => {
        var parsed_data = JSON.parse(data);
        onMessage(parsed_data);
      };
      return socket;
    }
  }
};
