import axios from 'axios';
import API_ENDPOINT from './endpoint';

axios.defaults.baseURL = API_ENDPOINT;

export default {
  login (credentials) {
    return axios.post('api-token-auth/', credentials).then((response) => {
      if (response.data.token) {
        localStorage.setItem('token', response.data.token);
      }
      return response;
    });
  },
  logout () {
    return Promise.resolve().then(() => {
      localStorage.removeItem('token');
    });
  },
  getInfo () {
    return axios.get('population/me');
  },
  isAuthenticated () {
    let token = localStorage.getItem('token');
    return token ? true : false;
  },
  getAuthHeader () {
    let token = localStorage.getItem('token');
    return token;
  },
  generateNormalPopulation (amount) {
    return axios.post('population/generate_population_normal/', amount);
  },
  generateUniformPopulation (amount) {
    return axios.post('population/generate_population_uniform/', amount);
  },
};
