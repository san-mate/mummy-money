# api_mummy_money

## Project setup
```
pip install -r requirements.txt
```

### Create database
```
python manage.py migrate
```

### Run server for development
```
python manage.py runserver
```

### Run tests
```
python manage.py test api_core/tests/
```
