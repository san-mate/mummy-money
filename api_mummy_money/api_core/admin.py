from django.contrib import admin

from api_core.models import Person, Week

admin.site.register(Person)
admin.site.register(Week)
