from django.conf import settings
from django.contrib.auth.models import User, Group

from rest_framework import serializers

from api_core.models import Person, Week


class PersonReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Person
        depth = 1
        fields = ('id', 'user', 'recruiter', 'innocence', 'experience', 'charisma')


class WeekReadSerializer(serializers.Serializer):
    new_members = PersonReadSerializer(many=True, read_only=True)
    members_leaving = PersonReadSerializer(many=True, read_only=True)

    class Meta:
        model = Week
        depth = 1
        fields = ('members_leaving', 'new_members')


class WeekSimulatorSerializer(serializers.Serializer):
    weeks = serializers.IntegerField()


class GeneratePopulationSerializer(serializers.Serializer):
    amount = serializers.IntegerField()


class DashboardSerializer(serializers.ModelSerializer):
    new_members = PersonReadSerializer(many=True, read_only=True)
    members_leaving = PersonReadSerializer(many=True, read_only=True)

    total_population = serializers.SerializerMethodField()
    mummy_money_earned_per_week = serializers.SerializerMethodField()
    total_members_per_week = serializers.SerializerMethodField()
    avg_money_member_per_week = serializers.SerializerMethodField()
    past_weeks = serializers.SerializerMethodField()

    class Meta:
        model = Week
        fields = [
            'new_members',
            'members_leaving',
            'total_population',
            'total_members_per_week',
            'mummy_money_earned_per_week',
            'avg_money_member_per_week',
            'past_weeks'
        ]

    def get_past_weeks(self, obj):
        return ['Week %s' % id for id in Week.objects.all().values_list('id', flat=True)]

    def get_total_population(self, obj):
        return Person.objects.all().count()

    def get_mummy_money_earned_per_week(self, obj):
        response = []
        for week in Week.objects.all():
            response.append(week.new_members.all().count() * settings.PROFIT_TO_MUMMY)
        return response

    def get_total_members_per_week(self, obj):
        response = []
        for week in Week.objects.all():
            response.append(week.new_members.all().count())
        return response

    def get_avg_money_member_per_week(self, obj):
        response = []
        for week in Week.objects.all():
            response.append(week.new_members.all().count() * settings.PROFIT_TO_MEMBER)
        return response
