import math

from django.conf import settings
from django.contrib.auth.models import User
from django.db import models


class Person(models.Model):
    innocence = models.FloatField()
    experience = models.FloatField()
    charisma = models.FloatField()
    active_member = models.BooleanField(default=False)
    inactive_weeks = models.IntegerField(default=0)

    recruiter = models.ForeignKey('self', on_delete=models.CASCADE, null=True, related_name='recruited_members')
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')

    def probability_to_recruit(self):
        if self.recruited_members.count() > 0:
            return self.experience * self.charisma * (1-math.log(self.recruited_members.count(), 10))
        else:
            return self.experience * self.charisma

    def probability_to_accept(self):
        return self.innocence * (1 - self.experience)

    def weeks_limit(self):
        return math.floor((1- self.innocence) * self.experience * self.charisma * 10)

    def money_earned(self):
        return self.recruited_members.all().count() * settings.PROFIT_TO_MEMBER


class Week(models.Model):
    total_members = models.ManyToManyField(Person, related_name='weeks_participating')
    members_leaving = models.ManyToManyField(Person, related_name='week_leaving')
    new_members = models.ManyToManyField(Person, related_name='week_entering')

    class Meta:
        ordering = ['id']

    def get_current_week():
        return Week.objects.all().last()
