from channels.generic.websocket import JsonWebsocketConsumer

from api_core.serializers import DashboardSerializer
from api_core.models import Week


class DashboardConsumer(JsonWebsocketConsumer):
    def connect(self):
        self.accept()
        serializer = DashboardSerializer(instance=Week.get_current_week())
        self.send_json(serializer.data)

    def disconnect(self, close_code):
        pass

    def receive_json(self, content):
        message = content['message']

        self.send_json(text_data={
            'message': message
        })
