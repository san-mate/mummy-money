import random

from django.contrib.auth.models import User, Group
from django.contrib.auth.hashers import make_password

from rest_framework import viewsets
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response

from numpy.random import uniform
from scipy.stats import truncnorm

from api_core.utils import get_random_string
from api_core.models import Person, Week
from api_core.serializers import PersonReadSerializer, WeekReadSerializer, \
    WeekSimulatorSerializer, GeneratePopulationSerializer


class PersonViewSet(viewsets.ModelViewSet):
    queryset = Person.objects.all()
    serializer_class = PersonReadSerializer

    @action(methods=['GET'], detail=False)
    def me(self, request, pk=None):
        serializer = self.serializer_class(request.user.profile, context=self.get_serializer_context())
        return Response(
            serializer.data,
            status=200,
            headers=self.get_success_headers(serializer.data)
        )

    @action(methods=['POST'], detail=False)
    def generate_population_uniform(self, request, pk=None):
        serializer = GeneratePopulationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        amount = serializer.validated_data['amount']

        innocence_attrs = uniform(0, 1, amount)
        experience_attrs = uniform(0, 1, amount)
        charisma_attrs = uniform(0, 1, amount)
        self.create_persons(amount, innocence_attrs, experience_attrs, charisma_attrs)
        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(methods=['POST'], detail=False)
    def generate_population_normal(self, request, pk=None):
        serializer = GeneratePopulationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        amount = serializer.validated_data['amount']

        mean, sd, low, upper = 0.5, 0.01, 0, 1
        innocence_attrs = truncnorm((low - mean) / sd, (upper - mean) / sd, loc=mean, scale=sd).rvs(amount)
        experience_attrs = truncnorm((low - mean) / sd, (upper - mean) / sd, loc=mean, scale=sd).rvs(amount)
        charisma_attrs = truncnorm((low - mean) / sd, (upper - mean) / sd, loc=mean, scale=sd).rvs(amount)
        self.create_persons(amount, innocence_attrs, experience_attrs, charisma_attrs)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def create_persons(self, amount, innocence_attrs, experience_attrs, charisma_attrs):
        candidates_group = Group.objects.get(name='Candidates')
        for i in range(amount):
            username = get_random_string(6)
            new_user = User.objects.create(
                username=username,
                password=make_password(username)
            )
            new_user.groups.add(candidates_group)
            Person.objects.create(
                innocence=innocence_attrs[i],
                experience=experience_attrs[i],
                charisma=charisma_attrs[i],
                user=new_user
            )


class WeekViewSet(viewsets.ModelViewSet):
    queryset = Week.objects.all()
    serializer_class = WeekReadSerializer

    @action(methods=['POST'], detail=False)
    def simulate_weeks(self, request, pk=None):
        serializer = WeekSimulatorSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        number_of_weeks = serializer.validated_data['weeks']

        investors_group = Group.objects.get(name='Investors')
        candidates_group = Group.objects.get(name='Candidates')

        if Week.objects.all().count() == 0 and Person.objects.exclude(id=1).count() >= 10:
            mummy = User.objects.get(id=1)
            first_investors_ids = random.sample(
                set(User.objects.exclude(id=1).values_list('id', flat=True)), 10)
            first_investors = User.objects.filter(id__in=first_investors_ids)
            persons_investors = Person.objects.filter(user__in=first_investors.all())
            persons_investors.update(
                recruiter=mummy,
                active_member=True
            )
            investors_group.user_set.set(first_investors)
            week = Week.objects.create()
            week.new_members.set(persons_investors)

        for n in range(number_of_weeks):
            new_week = Week.objects.create()
            members_leaving = []
            new_members = []
            for member in Person.objects.filter(active_member=True, user__groups=investors_group):
                x = random.uniform(0, 1)
                if x > member.probability_to_recruit():
                    candidates_count = Person.objects.filter(
                        user__groups=candidates_group).count()
                    if candidates_count == 0:
                        break
                    candidate = Person.objects.filter(
                        user__groups=candidates_group
                    ).all()[random.randint(0, candidates_count - 1)]
                    y = random.uniform(0, 1)
                    if y > candidate.probability_to_accept():
                        candidate.recruiter = member
                        candidate.active_member = True
                        candidate.user.groups.remove(candidates_group)
                        candidate.user.groups.add(investors_group)
                        candidate.save()
                        new_members.append(candidate)
                else:
                    member.inactive_weeks += 1
                    if member.inactive_weeks == member.weeks_limit():
                        member.active_member = False
                        members_leaving.append(member)
                    member.save()
            new_week.new_members.set(new_members)
            new_week.members_leaving.set(members_leaving)

        return Response(status=status.HTTP_204_NO_CONTENT)
