from django.urls import reverse_lazy

from rest_framework.test import APITestCase

from api_core.models import Person

from base_tests import PersonMixin


class TestPersons(PersonMixin, APITestCase):

    persons_list_url = reverse_lazy('person-list')
    persons_generate_population_normal_url = reverse_lazy('person-generate-population-normal')
    persons_generate_population_uniform_url = reverse_lazy('person-generate-population-uniform')

    def test_normal_population_creation(self):
        http_authorization = self.get_authorization_header(self.mummy)
        data = {'amount': 20}
        response = self.client.post(
            self.persons_generate_population_normal_url,
            HTTP_AUTHORIZATION=http_authorization,
            data=data
        )
        self.assertEquals(response.status_code, 204)
        # 20 new users + mummy
        self.assertTrue(Person.objects.all().count() == 21)

    def test_uniform_population_creation(self):
        http_authorization = self.get_authorization_header(self.mummy)
        data = {'amount': 20}
        response = self.client.post(
            self.persons_generate_population_normal_url,
            HTTP_AUTHORIZATION=http_authorization,
            data=data
        )
        self.assertEquals(response.status_code, 204)
        # 20 new users + mummy
        self.assertTrue(Person.objects.all().count() == 21)
