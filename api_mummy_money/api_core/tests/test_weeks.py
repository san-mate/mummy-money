from django.urls import reverse_lazy

from rest_framework.test import APITestCase

from api_core.models import Week, Person

from base_tests import PersonMixin


class TestPersons(PersonMixin, APITestCase):

    weeks_list_url = reverse_lazy('week-list')
    simulate_weeks_url = reverse_lazy('week-simulate-weeks')

    def test_simulation_with_less_than_10(self):
        for n in range(5):
            self.create_candidate()
        http_authorization = self.get_authorization_header(self.mummy)
        data = {'weeks': 3}
        response = self.client.post(
            self.simulate_weeks_url,
            HTTP_AUTHORIZATION=http_authorization,
            data=data
        )
        self.assertEquals(response.status_code, 204)
        # Cant create initial week without at least 10 Candidates
        self.assertTrue(Week.objects.all().count(), 0)

    def test_simulation_with_10_users(self):
        for n in range(10):
            self.create_candidate()
        # Validate de amount of users
        self.assertTrue(Person.objects.all().count() == 11)

        http_authorization = self.get_authorization_header(self.mummy)
        data = {'weeks': 3}
        response = self.client.post(
            self.simulate_weeks_url,
            HTTP_AUTHORIZATION=http_authorization,
            data=data
        )
        self.assertEquals(response.status_code, 204)
        # First week should have 10 new_members
        self.assertEquals(Week.objects.all().first().new_members.all().count(), 10)
        # Number of weeks expecting N + the initial
        self.assertEquals(Week.objects.all().count(), 4)

    def test_simulation_with_10_users_0_weeks(self):
        for n in range(10):
            self.create_candidate()
        http_authorization = self.get_authorization_header(self.mummy)
        data = {'weeks': 0}
        response = self.client.post(
            self.simulate_weeks_url,
            HTTP_AUTHORIZATION=http_authorization,
            data=data
        )
        self.assertEquals(response.status_code, 204)
        # First week should have 10 new_members
        self.assertTrue(Week.objects.all().first().new_members.all().count(), 10)
        # Number of weeks expecting N + the initial = 1
        self.assertTrue(Week.objects.all().count() == 1)

    def test_simulation_with_15_users(self):
        for n in range(15):
            self.create_candidate()
        http_authorization = self.get_authorization_header(self.mummy)
        data = {'weeks': 4}
        response = self.client.post(
            self.simulate_weeks_url,
            HTTP_AUTHORIZATION=http_authorization,
            data=data
        )
        self.assertEquals(response.status_code, 204)
        # First week should have 10 new_members
        self.assertTrue(Week.objects.all().first().new_members.all().count(), 10)
        # Number of weeks expecting N + the initial
        self.assertTrue(Week.objects.all().count() == 5)

    def test_terminate_program(self):
        pass
