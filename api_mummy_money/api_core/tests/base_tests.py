from django.contrib.auth.models import Group, User
from django.contrib.auth.hashers import make_password

from rest_framework.authtoken.models import Token

from api_core.models import Person
from api_core.utils import get_random_string


class PersonMixin(object):

    def setUp(self):
        super(PersonMixin, self).setUp()
        self.create_groups()
        self.create_users()

    def create_groups(self):
        self.investors_group, _ = Group.objects.get_or_create(name='Investors')
        self.candidates_group, _ = Group.objects.get_or_create(name='Candidates')

    def create_users(self):
        self.mummy, _ = User.objects.get_or_create(
            username='mummy'
        )
        self.mummy_person, _ = Person.objects.get_or_create(
            user=self.mummy,
            active_member=True
        )
        self.mummy.set_password('mummy')

    def get_access_toke(self, user):
        token, created = Token.objects.get_or_create(user=user)
        return token

    def get_authorization_header(self, user):
        access_token = self.get_access_toke(user)
        return "Token {0}".format(access_token.key)

    def create_candidate(self):
        username = get_random_string(6)
        new_user = User.objects.create(
            username=username,
            password=make_password(username)
        )
        new_user.groups.add(self.candidates_group)
        Person.objects.create(
            user=new_user,
            innocence=0,
            experience=0,
            charisma=0,
            active_member=True
        )
