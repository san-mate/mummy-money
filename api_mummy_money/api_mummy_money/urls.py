from django.contrib import admin
from django.urls import path, include

from rest_framework.routers import DefaultRouter
from rest_framework.authtoken import views

from api_core.resources import PersonViewSet, WeekViewSet

router = DefaultRouter()
router.register(r'population', PersonViewSet)
router.register(r'weeks', WeekViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('api-token-auth/', views.obtain_auth_token),
    path('admin/', admin.site.urls),
]
