from channels.routing import ProtocolTypeRouter, URLRouter

from api_core.token_auth import TokenAuthMiddlewareStack
import api_core.routing


application = ProtocolTypeRouter({
    # (http->django views is added by default)
    "websocket": TokenAuthMiddlewareStack(
        URLRouter(
            api_core.routing.websocket_urlpatterns
        ),
    ),
})
