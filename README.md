# Mummy Money Program

## Run Project
```
docker-composea up
```

## Initial build only:

Create initial database:

```
docker-compose run api python manage.py migrate
```

### Initial Users and Data

After running the migrations, the system will have a "Mummy" user admin (username: mummy, pass: mummy), with which you can login at:

- [localhost:8000/admin](localhost:8000/admin)
- [localhost:8080/login](localhost:8080/login)

Also there should be 2 Groups:

1. Candidates. These are the users that can be recruited by the Investors, also the initial state for all the generated populations.
2. Investors. These users are part of the program and can access to the Dashboard.

## Usage

From the Dashboard the Mummy can create a population of Candidates using two different methods:

- Normal Random Distribution to fill the attributes of the Candidates.
- Uniform Random Distribution to fill the attributes of the Candidates.

To run a simulation of N weeks it is required a minimun number of 10 Candidates that will be picked as initial Investors.
The first week on the system will be filled with the recruitment of the initial Investors, so after running a simulation there should show N + 1 weeks on the system.

Whenever the Mummy decides to terminate the program, this action will remove all the Investors, taking away their access to the system too.

### Tests

The API has a module with tests that can be runned with:

```
docker-compose run api python manage.py test api_core/tests/
```

## Components

### API

The API module (api_mummy_money) serves a Django app with Django Rest Framework and Chanels, and is configured to be available at: [localhost:8000](localhost:8000)

### WEB

The WEB module (web_mummy_money) serves the frontend app with Vue, and is configured to be available at: [localhost:8080](localhost:8080)

The theme is based on [Vue Material Dashboard](https://github.com/creativetimofficial/vue-material-dashboard)

